resource "aws_ecs_service" "app" {
  name            = var.application
  cluster         = aws_ecs_cluster.ecs_cluster.id
  task_definition = aws_ecs_task_definition.tf-app.arn
  desired_count   = 2
  launch_type     = "FARGATE"

  network_configuration {
    assign_public_ip = true
    security_groups  = [aws_security_group.sg-wp-ecs.id]
    subnets          = data.aws_subnets.private_subnet.ids
  }

  load_balancer {

    target_group_arn = aws_lb_target_group.ecs-alb.arn
    container_name   = var.application
    container_port   = 80
  }
  depends_on = [ aws_ecs_task_definition.tf-app ]
}