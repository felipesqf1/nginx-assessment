
resource "aws_route53_record" "felipesqf" {
  zone_id = data.aws_route53_zone.felipesqf.zone_id
  name    = var.application
  type    = "CNAME"
  ttl     = "300"
  records = [aws_lb.ecs-alb.dns_name]
}