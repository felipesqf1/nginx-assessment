
resource "aws_ecs_task_definition" "tf-app" {
  family                   = var.application
  requires_compatibilities = ["FARGATE"]
  execution_role_arn       = aws_iam_role.task_def.arn
  task_role_arn            = aws_iam_role.task_def.arn
  network_mode             = "awsvpc"
  cpu                      = var.cpu
  memory                   = var.memory

  container_definitions = <<EOT
[
  {
    "name": "${var.application}",
    "image": "${var.image}",
    "memoryReservation": 1024,
    "essential": true,
    "portMappings": [
      {
        "hostPort": 80,
        "containerPort": 80,
        "protocol": "tcp"
      }
    ],
    "environment": [
      { "name": "NGINX_PORT", "value": "80" }
    ]
  }
]
EOT
}

