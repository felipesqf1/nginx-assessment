terraform {
  backend "s3" {
    bucket = "remote-state-final-challenge"
    region = "ap-southeast-2"
    key    = "terraform.tfstate-ngnix-assessment"
  }
}