# Terraform - Fargate - Nginx
This is an nginx application running on ECS Fargate containers. The whole application and infrastructure was deployed using terraform.<br><br>
​
This archtecture will create:<br>

- VPC<br>
- Subnets<br>
- Internet Gateway<br>
- NAT Gateway<br>
- S3 Bucket for the statefile <br>
- Application Load Balancer<br>
- Fargate Cluster<br>
- Fargate Service<br>
- Task Definition<br>
- Auto Scaling Group<br>


The VPC infrastructe is created using the following module:<br>
https://github.com/felipesqf/terraform-fargate



## Architecture
![Alt Text](https://gitlab.com/felipesqf1/nginx-assessment/-/raw/main/nginx.png)
<!-- ![screenshot1](https://github.com/felipesqf/terraform-fargate-application/blob/main/React-Node_new.png)  --> -->

## URL
- Application url: https://nginx.felipesqf.com

## Requirements
- AWS Account
- Route53 for domain record

## Future Improvements
- Add Codedeploy 
- Add WAF
- Improve CloudWatch 

## Authentication
Terraform uses the authentication configured in your AWS CLI, make sure you have access to your account configured in at least one profile before executing the deploy.


## Execution
- Configure AWS CLI
- Install Terraform
- Run terraform init
- Run terraform plan
- Run terraform apply


## Authors
Felipe Ferreira  <br><br>
Contact information:<br>
felipesqf@gmail.com<br>
Github: felipesqf<br>
+61 0406 021 252