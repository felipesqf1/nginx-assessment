resource "aws_iam_role" "task_def" {
  name = "task_def"
  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = "sts:AssumeRole"
        Effect = "Allow"
        Sid    = ""
        Principal = {
          Service = "ecs-tasks.amazonaws.com"
        }
      },
    ]
  })

}


# resource "aws_iam_role_policy" "tf_policy_rds" {
#   name = "Policy_TaskDefinitionRDS"
#   role = aws_iam_role.task_def.id

#   policy = jsonencode({
#     Version = "2012-10-17"
#     Statement = [
#       {
#         Action = [
#           "rds:*",
#         ]
#         Effect   = "Allow"
#         Resource = "${aws_db_instance.myrds.arn}"
#       },
#     ]
#     }
#   )
# }

# resource "aws_iam_role_policy" "tf_policy_efs" {
#   name = "Policy_TaskDefinitionEFS"
#   role = aws_iam_role.task_def.id
#   policy = jsonencode({
#     Version = "2012-10-17"
#     Statement = [
#       {
#         Action = [
#           "efs:*",
#         ]
#         Effect   = "Allow"
#         Resource = "${aws_efs_file_system.efs-ecs.arn}"
#       },
#     ]
#   })
# }


resource "aws_iam_role_policy" "tf_policy_ssm" {
  name = "Policy_TaskDefinitionSSM"
  role = aws_iam_role.task_def.id
  policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = [
          "ssm:GetParameters",
        ]
        Effect   = "Allow"
        Resource = "${var.parameter_arn}"
      },
    ]
  })
}

resource "aws_iam_role_policy" "tf_policy_cw" {
  name = "Policy_TaskDefinitionCloudWatch"
  role = aws_iam_role.task_def.id
  policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = [
          "logs:CreateLogStream",
          "logs:PutLogEvents"
        ]
        Effect   = "Allow"
        Resource = "*"
      },
    ]
  })
}
