# variable "vpc_id" {
#     # default = data.aws_vpc.vpc_id.id
# }

# variable "secure_subnets" {
#     # default = data.aws_subnet_ids.secure_subnets
# }

# variable "private_subnets" {
#     # default = data.aws_subnet_ids.private_subnets

# }
# variable "public_subnets" {
#     # default = data.aws_subnet_ids.public_subnets

# }

# variable "db_username" {

# }

# variable "db_tablename" {

# }

# variable "DB_name" {

# }
# variable "db_user" {

# }
# variable "rds_db_password" {

# }
# variable "iam_task_def" {

# }
variable "parameter_arn" {
  default = "arn:aws:ssm:ap-southeast-2:702713127235:parameter/rds/wordpressdb/*"
}

variable "application" {
  description = "Application name"
  default     = "nginx"
}

variable "cpu" {
  description = "CPU size"
  default     = 512

}
variable "memory" {
  description = "Memory size"
  default     = 1024

}
variable "image" {
  description = "Image"
  default     = "nginx:alpine3.18-slim"

}
variable "environment" {
  description = "Environment name"
  default     = "dev"

}
variable "port" {
  description = "Port"
  default     = "80"

}